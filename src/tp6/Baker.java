package tp6;

/*
 * The Baker class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * @author      Crausaz Romain <crausaz.romain@edu.hefr.ch>
 * @version     1.1                   
 * @since       2012-04-20
 *
 * Honor Code: I pledge that this program represents my own
 *   program code. I received help from no one in designing
 *   and debugging my program.
 *
 */

public class Baker implements Runnable {
  
    public void run() {
        while (true) {
           
            try {
                //Wait to access to the food
                SandwichGourmets.lockFood.acquire();
            }
            catch (InterruptedException e) {
                    e.printStackTrace();
            }
                
            //Try to take ham
            if(SandwichGourmets.ham.tryAcquire()){
                //Try to take butter
                if(SandwichGourmets.butter.tryAcquire()){
                    //Make the sandwich
                    System.out.println("Baker adds bread and makes sandwich");
                    System.out.println("Baker eats his sandwich");
                    SandwichGourmets.grocer_semaphore.release(); 
                }
                else{
                    //Release ham (ham.tryAcquire decrease by 1)
                    SandwichGourmets.ham.release();
                }
            }                            
            //Release the access to the food
            SandwichGourmets.lockFood.release();
        }
    }

}
