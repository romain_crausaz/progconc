package tp6;

/*
 * Description of your class. You should change this!
 * Some more comments
 * 
 * @author      Crausaz Romain <crausaz.romain@edu.hefr.ch>
 * @version     0.0                   
 * @since       2012-04-20
 *
 * Completion time: 1h25min
 *
 * Honor Code: I pledge that this program represents my own
 *   program code. I received help from no one in designing
 *   and debugging my program.
 *
 */

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;

public class SandwichGourmets {
    public static Semaphore grocer_semaphore  = new Semaphore(1, false);

    public static Semaphore bread             = new Semaphore(0, true);
    public static Semaphore ham               = new Semaphore(0, true);
    public static Semaphore butter            = new Semaphore(0, true);
    
    /*Mutex to lock the access to the food to only one Cooker (Baker, Butcher
      or Milkman)*/
    public static Semaphore lockFood          = new Semaphore(1 );
    
    public static ExecutorService threadExecutor = Executors
            .newCachedThreadPool();

    public static void main(String[] args) {
        Grocer grocer   = new Grocer();
        Baker baker     = new Baker();
        Butcher butcher = new Butcher();
        Milkman milkman = new Milkman();
        threadExecutor.execute(grocer);
        threadExecutor.execute(baker);
        threadExecutor.execute(butcher);
        threadExecutor.execute(milkman);
    }

}
