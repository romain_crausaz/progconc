package tp6;


/*
 * The Milkman class for the Sandwich Gourmet Problem
 * Concurrent Programming - TP6
 * 
 * @author      Crausaz Romain <crausaz.romain@edu.hefr.ch>
 * @version     1.1                   
 * @since       2012-04-20
 *
 * Honor Code: I pledge that this program represents my own
 *   program code. I received help from no one in designing
 *   and debugging my program.
 */

public class Milkman implements Runnable {

    @Override
    public void run() {
        while (true) {
            
            
            try {
                //Wait to access to the food
                SandwichGourmets.lockFood.acquire();
            }
            catch (InterruptedException e) {
                e.printStackTrace();
            }
                
            //Try to take the bread
            if(SandwichGourmets.bread.tryAcquire()){
                //Try to take the ham
                if(SandwichGourmets.ham.tryAcquire()){
                    //Make the sandwich
                    System.out.println("Milkman adds " +
                    		"butter and makes sandwich");
                    System.out.println("Milkman eats his sandwich");
                    SandwichGourmets.grocer_semaphore.release();
                }
                else{
                    //Release bread (bread.tryAcquire decrease by 1)
                    SandwichGourmets.bread.release();
                }
            }
            //Release the access to the food
            SandwichGourmets.lockFood.release();  
        }
    }
}
