/**
 * 
 * The Class "Client" is a thread that simulates a customer ; it manage
 * the time before that a customer decides to do a ride, the duration of
 * his emotions. And the fact of wanting to board or unboard of the train.
 * 
 * 
 * @authors     Harold Brulhart <harold.bruelhart@edu.hefr.ch>
 * 				Romain Crausaz 	<romain.crausaz@edu.hefr.ch>
 * @version     1.2                   
 * @since     	2012-05-29
 *
 * Completion time : 25 minutes.
 * 
 * Honor Code: We pledge that this program represents our own
 *             program code. We received help from no one in designing
 *             and debugging our program.
 * 
 */

import java.util.Random;


public class Client implements Runnable{
	// Duration min and max for a customer decides to do his first ride
    final static private int FIRST_TOUR_MIN_TIME = 1000; 
    final static private int FIRST_TOUR_MAX_TIME = 10000;
    
    // Duration min and max for a customer decides to do another ride
    final static private int NEXT_TOUR_MIN_TIME = 20000;
    final static private int NEXT_TOUR_MAX_TIME = 30000;
    
    // Duration min and max of a emotion
    final static int EMOTION_MIN_TIME = 1000;
    final static int EMOTION_MAX_TIME = 2000;
    
    private boolean first_tour; 
    
    private Random randomGen = new Random();
    
    // Types of emotion
    private String Emotions[] = {"closing eyes", "shouting", "waving arms"}; 
    
    String name;
    
    public Client(String name) {
        this.name = name; // Name of customer
        first_tour = true; // Flag for his first Roller Coaster ride
    }

    public void run() {
        while(true){
            try{
                if(first_tour){
                	// Random time before the customer looks to do
                	// his first ride of the Roller Coaster
                    Thread.sleep(FIRST_TOUR_MIN_TIME + 
                                 randomGen.nextInt(FIRST_TOUR_MAX_TIME -
                                                   FIRST_TOUR_MIN_TIME)); 
                    first_tour = false; // Indicates that the first ride is done
                    
                } else {
                	// Random time before the customer looks to do
                	// another ride of the Roller Coaster
                    Thread.sleep(NEXT_TOUR_MIN_TIME + 
                            randomGen.nextInt(NEXT_TOUR_MAX_TIME -
                                              NEXT_TOUR_MIN_TIME));  
                }
                // The customer wants to board on the train
                RollerCoaster.monitor.board(this);
                
                // While the train rides
                while(!RollerCoaster.monitor.train_stop){
                	// Random time for the duration of a emotion
                     Thread.sleep(EMOTION_MIN_TIME +
                             randomGen.nextInt(EMOTION_MAX_TIME - 
                                               EMOTION_MIN_TIME));
                     
                     // Attributes and displays the emotion of the customer
                     // (random choice)
                     if(!RollerCoaster.monitor.train_stop){
                         System.out.println("! " + name + " is " + 
                             Emotions[randomGen.nextInt(Emotions.length)]);
                     }
                }
                // The customer unboard of the train
                RollerCoaster.monitor.unboard(this); 
                
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
