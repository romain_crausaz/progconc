/**
 * 
 * The Class "Roller Coaster" is the main class of the application,
 * it activates the train (thread) and customers (thread), it also 
 * attributes the names of customers (who are in the text file names.txt
 * at the root of the application).
 * 
 * 
 * @authors     Harold Brulhart <harold.bruelhart@edu.hefr.ch>
 * 				Romain Crausaz 	<romain.crausaz@edu.hefr.ch>
 * @version     1.2                   
 * @since       012-05-29
 *
 * Completion time : 25 minutes.
 * 
 * Honor Code: We pledge that this program represents our own
 *             program code. We received help from no one in designing
 *             and debugging our program.
 * 
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RollerCoaster {
    // Creation of the thread executor
    public static ExecutorService thExecutor = Executors.newCachedThreadPool();  
    
    public static Monitor monitor = new Monitor(); // Creation of the monitor
    
    // Number of customers for the Roller Coaster
    final static int NB_OF_CLIENTS = 20; 
    
    public static void main(String[] args) {
        String name;
        // Text file (for the attribution of names)
        File inFile = new File("names.txt");
        try {
            Scanner in = new Scanner(inFile);
            // Creation of the train
            Train train = new Train(); 
            // Creation of customers
            Client clients[] = new Client[NB_OF_CLIENTS]; 
            
            thExecutor.execute(train); // Activates the train (start the thread)
            for (int i = 0; i < NB_OF_CLIENTS; i++) { // For each customers
                name = in.nextLine(); // Takes a name in the text file
                // Attributes the name at the customer
                clients[i] = new Client(name); 
                // Activates customers (start the thread)
                thExecutor.execute(clients[i]); 
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
