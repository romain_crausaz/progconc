/**
 * 
 * The Class "Monitor" is the monitor of the application, this is it
 * which manages critical sections of the application.
 * 
 * @authors     Harold Brulhart <harold.bruelhart@edu.hefr.ch>
 * 				Romain Crausaz 	<romain.crausaz@edu.hefr.ch>
 * @version     1.2                   
 * @since       2012-05-29
 *
 * Completion time : 80 minutes.
 * 
 * Honor Code: We pledge that this program represents our own
 *             program code. We received help from no one in designing
 *             and debugging our program.
 * 
 */

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class Monitor {
    
	// Number max of customers in the train
    final static private long MAX_CLIENTS = 6;
    
    // For the critical section
    private final Lock lock = new ReentrantLock();
    
    // To allows customers to go on the train or not
    private final Condition Client_boarding = lock.newCondition(); 
    // To allows customers to go out of the train or not
    private final Condition Client_unboarding = lock.newCondition();
    // To allows the train to ride or not
    private final Condition Train_wait = lock.newCondition();
    // To block customers who already boarded in the train
    // prevents them to unboard before the ride finish
    private final Condition Client_board = lock.newCondition(); 
    
    private int nb_clients; // Number of customers
    private boolean train_ready; // The train is ready (true)
    public boolean train_stop; // The train is stationary (true)
 
    
    public Monitor(){
        nb_clients = 0; 
        train_ready = true; 
        train_stop = false; 
        
    }
    
    // Method 	 : 	load_train()
    // Parameter :	-
    // Function  :	For loading the train (authorization)
    public void load_train() throws InterruptedException {
        lock.lock(); // Critical section
        try{
            System.out.println("Train ready");
            train_ready = true; // The is ready to be loaded
            Client_boarding.signalAll(); // Customers can go on the train
            Train_wait.await(); // The train is stationary (lock)
                                // waiting that the last customers has boarded
            Client_board.signalAll(); // Train starting, customers 
                                      // can have emotion
            System.out.println("--- Train departing --->"); 
            train_stop = false; // The train is not stationary
        } finally {
            lock.unlock(); // Releases the critical section
        }
    }
    
    // Method 	 : 	unload_train()
    // Parameter :	-
    // Function  :  For unloading the train (authorization)
    public void unload_train() throws InterruptedException {
        lock.lock(); // Critical section
        try{
            train_stop = true; // The train is stationary
            System.out.println("<--- Train finished ---");
            // Customers can go out of the train
            Client_unboarding.signalAll(); 
            Train_wait.await(); // Train is stationary (lock)
        } finally {
            lock.unlock(); // Releases the critical section
        }
    }
     
    // Method 	 : 	board(Client c)
    // Parameter :	Client (customer)
    // Function  :	Manages the boarding of customers
    public void board(Client c) throws InterruptedException {
        lock.lock(); // Critical section
        try{
            System.out.println(c.name + " wants a ride.");
            while(!train_ready){ // While the train is not ready to be loaded
                if(!train_stop){ // If the train is not stationary
                    System.out.println(c.name + " waiting for the train to come");
                }
                // Customer is waiting for a ride (lock)
                Client_boarding.await();
            }
            nb_clients++; 
            System.out.println("> " + c.name + " boarding");
            // If the current customer is the last customer who is authorized 
            // to go on the train (6th).
            if(nb_clients == MAX_CLIENTS){ 
                train_ready = false; // The train is not ready to be loaded
                System.out.println(c.name + " is the last customer to board!");
                Train_wait.signal(); // The train will ride 
            }
            Client_board.await(); // Customer wait the train to start
        } finally {
            lock.unlock(); // Releases the critical section
        }
    }
    
    // Method 	 : 	unboard(Client c)
    // Parameter :	Client (customer)
    // Function  :	Manages the unboarding of customers
    public void unboard(Client c) throws InterruptedException {
       lock.lock(); // Critical section
       try{
           while(!train_stop){ // While the train ride
        	   // Customer can't get out of the train
               Client_unboarding.await(); 
           }
           nb_clients--; 
           System.out.println("< " + c.name + " is unboarding");
           if(nb_clients == 0){ // If the train is empty
               System.out.println(c.name + " is the last customer to leave!");
               System.out.println("Train empty");
               Train_wait.signal(); // The train is ready to load customer
           }
       } finally {
           lock.unlock(); // Releases the critical section
       }
    }
}
