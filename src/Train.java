/**
 * 
 * The Class "Train" is a thread that simulates the train of the
 * "Roller Coaster". In sum, we load the train (with clients), then
 * it ride a turn and finally we unload the train, and restart.
 * 
 * @authors     Harold Brulhart <harold.bruelhart@edu.hefr.ch>
 * 			    Romain Crausaz 	<romain.crausaz@edu.hefr.ch>
 * @version     1.2                   
 * @since       2012-05-29
 *
 * Completion time : 15 minutes.
 * 
 * Honor Code: We pledge that this program represents our own
 *             program code. We received help from no one in designing
 *             and debugging our program.
 */

public class Train implements Runnable{
	
    final static private long TOUR_TIME = 5500; // Time for a ride (5.5s)
    
    public void run() {
        while(true){ // Infinite loop
            try{
                RollerCoaster.monitor.load_train(); // Load the train
                Thread.sleep(TOUR_TIME); // Roller Coaster ride (5.5s)
                RollerCoaster.monitor.unload_train(); // Unload the train    
            } catch (InterruptedException e){
                e.printStackTrace(); 
            }
        }
    } 
}
